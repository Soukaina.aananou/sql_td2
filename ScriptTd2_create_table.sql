-- Création de la table rum --

CREATE TABLE rum (
id_rum serial not null primary key,
id_sejour INT,
date_entree DATE NOT NULL,
date_sortie DATE NOT NULL,
mode_admission VARCHAR(100) NOT NULL,
mode_sortie VARCHAR(100) NOT null,
FOREIGN KEY (id_sejour) REFERENCES sejour (id_sejour)
)
;

-- Création de la table ACTE --

CREATE TABLE acte (
id_acte serial not null primary key,
libelle_acte VARCHAR(50) NOT NULL,
code_acte VARCHAR(50) NOT NULL
)
;

-- Création de la table DIAGNOSTIC --

CREATE TABLE diagnostic (
id_diagnostic serial not null primary key,
libelle_diagnostic VARCHAR(50) NOT NULL,
code_diagnostic VARCHAR(50) NOT null
)
;

-- Création de la table rum_acte --

CREATE TABLE rum_acte (
id_rum INT NOT NULL,
id_acte INT NOT NULL,
date_acte DATE NOT NULL,
FOREIGN KEY (id_rum) REFERENCES RUM (id_rum),
FOREIGN KEY (id_acte) REFERENCES ACTE (id_acte)
)
;

-- Création de la table RUM_DIAGNOSTIC --

CREATE TABLE rum_diagnostic (
id_rum INT NOT NULL,
id_diagnostic INT NOT NULL,
date_diagnostic DATE NOT NULL,
FOREIGN KEY (id_rum) REFERENCES RUM (id_rum),
FOREIGN KEY (id_diagnostic) REFERENCES DIAGNOSTIC (id_diagnostic)
)
;

-- Modification de la nature des colonnes date_debut et date_fin importées des csv acte et diagnostic car elles se sont télécharger en varchar ce qui peut poser problème par la suite --

ALTER TABLE "soukaina.aananou.etu".acte ALTER COLUMN date_debut_validite TYPE date USING date_debut_validite::date;

ALTER TABLE "soukaina.aananou.etu".acte ALTER COLUMN date_fin_validite TYPE date USING date_fin_validite::date;

ALTER TABLE "soukaina.aananou.etu".diagnostic ALTER COLUMN date_debut_validite TYPE date USING date_debut_validite::date;

ALTER TABLE "soukaina.aananou.etu".diagnostic ALTER COLUMN date_fin_validite TYPE date USING date_fin_validite::date;

---------

-- Création d'enregistrement dans les tables --

INSERT INTO rum (id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
 VALUES
 (1, '2020-01-01', '2020-01-10', 'Urgences', 'HAD'),
 (2, '2020-01-01', '2020-01-12', 'Chir_Ambulatoire', 'HAD'),
 (3, '2020-01-20', '2020-01-25', 'Urgences', 'SL'),
 (4, '2020-01-01', '2020-01-08', 'Urgences', 'HAD'),
 (5, '2020-02-02', '2020-02-04', 'Chir_Ambulatoire', 'HAD');

INSERT INTO rum_acte (id_rum, id_acte, date_acte)
 VALUES
 (1, 1, '2020-01-03'),
 (2, 2, '2020-01-05'),
 (3, 3, '2020-01-21'),
 (4, 4, '2020-01-02'),
 (5, 5, '2020-02-02');

INSERT INTO rum_diagnostic (id_rum, id_diagnostic, date_diagnostic)
 VALUES
 (1, 1, '2020-01-01'),
 (2, 2, '2020-01-01'),
 (3, 3, '2020-01-20'),
 (4, 4, '2020-01-01'),
 (5, 5, '2020-02-02');






