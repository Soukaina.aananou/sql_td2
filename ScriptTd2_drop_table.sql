-- Suppresion des tables liés à d'autres tables en premier --

DROP TABLE rum_acte;
DROP TABLE rum_diagnostic;
DROP TABLE rum;

-- Puis, suppression des autres tables --

DROP TABLE acte;
DROP TABLE diagnostic;